package bellsimlib

import (
	"errors"
	"fmt"
	"io"
)

// SensorInterface defines the characteristics of the sensor interface
type SensorInterface struct {
	sensorPort  io.Reader // Port to read from
	bells       []Bell    // Array of bells
	BellHandler           // To store the bell handler
}

// BellHandler interface defines the bell handler
type BellHandler interface {
	Handle(*Bell)
}

// NewSensorInterface function is a constructor for sensorInterface
func NewSensorInterface(sensorPort io.Reader, bells []Bell, handler BellHandler) (SensorInterface, error) {
	var si SensorInterface
	var maxBells = 12

	// Perform validity checks and return error accordingly
	if sensorPort == nil {
		return si, errors.New("bellsimlib: sensorPort must be defined")
	}
	if len(bells) < 1 {
		return si, errors.New("bellsimlib: There must be at least one bell defined")
	}
	if len(bells) > maxBells {
		errorMsg := fmt.Sprintf("bellsimlib: The maximum number of bells that can be defined is %d", maxBells)
		return si, errors.New(errorMsg)
	}
	if handler == nil {
		return si, errors.New("bellsimlib: handler must be defined")
	}

	si = SensorInterface{sensorPort, bells, handler}
	return si, nil
}

// Start opens the Sensor interface device and starts reading
func (si *SensorInterface) Start() error {
	if si == nil {
		return errors.New("bellsimlib: SensorInterface is nil")
	}
	done := make(chan bool)
	go si.readAndHandle(done)
	<-done
	return nil
}

func (si *SensorInterface) readAndHandle(done chan bool) {
	var err error

	buf := make([]byte, 1)
	//log.Printf("Handler started\n")
	for {
		_, err = si.sensorPort.Read(buf)
		if err == io.EOF {
			break
		}
		//log.Printf("Character received: %s %x\n", string(buf), buf)

		// When character arrives, match to a bell and call handler
		for i := range si.bells {
			if si.bells[i].Sensor.Identifier == buf[0] {
				si.Handle(&si.bells[i])
				break
			}
		}
		si.Handle(nil) // Send null bell. Handler should normally ignore
	}
	done <- true
}
