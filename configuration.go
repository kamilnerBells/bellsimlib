package bellsimlib

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Configuration - This holds the configuration for the system
type Configuration struct {
	SensorPort struct {
		Device   string `yaml:"device"`
		BaudRate uint   `yaml:"baudRate"`
		DataBits uint   `yaml:"dataBits"`
		StopBits uint   `yaml:"stopBits"`
		MinRead  uint   `yaml:"minRead"`
	} `yaml:"sensorPort"`
	SynthSoundfont string `yaml:"synthSoundfont"`
	SynthLatency   uint   `yaml:"synthLatency"`
	Bells          []struct {
		Name        string `yaml:"name"`
		SensorChar  string `yaml:"sensorChar"`
		SensorDelay uint   `yaml:"sensorDelay"`
		Note        uint8  `yaml:"note"`
	} `yaml:"bells"`
}

// SaveConfig - Saves Simulator configuration
func SaveConfig(c Configuration, filename string) error {
	bytes, err := yaml.Marshal(c)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(filename, bytes, 0644)
}

// LoadConfig - Loads Simulator configuration
func LoadConfig(filename string) (Configuration, error) {
	fmt.Println("bellsimlib: Loading config file " + filename)
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return Configuration{}, err
	}

	var c Configuration
	err = yaml.Unmarshal(bytes, &c)
	if err != nil {
		return Configuration{}, err
	}

	return c, nil
}
