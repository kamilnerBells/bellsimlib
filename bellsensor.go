package bellsimlib

import "errors"

var (
	// ValidMbiChars is a map of valid characters for MBI
	ValidMbiChars = map[byte]bool{
		'1': true,
		'2': true,
		'3': true,
		'4': true,
		'5': true,
		'6': true,
		'7': true,
		'8': true,
		'9': true,
		'0': true,
		'E': true,
		'T': true,
		'A': true,
		'B': true,
		'C': true,
		'D': true,
	}
)

// BellSensor defines the characteristics of a bell sensor
type BellSensor struct {
	Identifier byte // This is the character received on the interface
	Delay      uint // Delay in ms from when the sensor receives the character to when the bell strikes
}

// NewBellSensor function is a constructor for Sensor
func NewBellSensor(identifier byte, delay uint) (*BellSensor, error) {

	var bs *BellSensor
	/*
	 * Checks to perform:
	 *  1. Identifier needs to be valid set of characters
	 		 { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'E', 'T', 'A', 'B', 'C', 'D' }
	 *  2. Delay has to be a sensible value (TBA)
	*/
	if !IsValidMbiChar(identifier) {
		return bs, errors.New("bellsimlib: Bell Sensor identifier is not a valid character")
	}
	return &BellSensor{identifier, delay}, nil
}

// IsValidMbiChar checks whether the specified character is valid
// This is based on the expected values sent by the Bagley Multi-Bell Interface
// and other hardware based on this.
func IsValidMbiChar(char byte) bool { return ValidMbiChars[char] }
