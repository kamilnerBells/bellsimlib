package bellsimlib

import (
	"io"
	"log"
	"strings"
	"testing"
)

func TestNewSensorInterface(t *testing.T) {

	mr := mockRinger{}

	var bells1 [2]Bell  // Define 2 bells
	var bells2 []Bell   // Empty array
	var bells3 [13]Bell // Oversize array

	sensor1, _ := NewBellSensor('1', 30)
	sensor2, _ := NewBellSensor('2', 30)

	bell1, _ := NewBell("1", sensor1, mr)
	bell2, _ := NewBell("2", sensor2, mr)

	bells1[0] = *bell1
	bells1[1] = *bell2

	// Define a simple stub port
	var stubPort1 io.Reader
	stubPort1 = strings.NewReader("This is a stub sensorPort")

	resultChan := make(chan *Bell)
	mockHandler := newMockHandler(resultChan)

	// First check that a correctly specified SensorInterface gets created properly
	si1, e := NewSensorInterface(stubPort1, bells1[:], mockHandler)
	if e != nil {
		t.Error("SensorInterface did not get correctly created")
	}
	if si1.sensorPort != stubPort1 {
		t.Errorf("sensorPort was incorrect, expected: %v, actual: %v", stubPort1, si1.sensorPort)
	}
	if len(si1.bells) != len(bells1) {
		t.Errorf("bells was incorrect, expected length: %d, actual: %d", len(bells1), len(si1.bells))
	}
	if si1.BellHandler == nil {
		t.Errorf("Handle was nil")
	}

	// Now check that validity checking is working
	_, e = NewSensorInterface(nil, bells1[:], mockHandler)
	if e == nil {
		t.Errorf("Validity checking of sensorPort is not working")
	}

	_, e = NewSensorInterface(stubPort1, nil, mockHandler)
	if e == nil {
		t.Errorf("Validity checking of bells is not working for nil array")
	}

	_, e = NewSensorInterface(stubPort1, bells2[:], mockHandler)
	if e == nil {
		t.Errorf("Validity checking of bells is not working for empty bells array")
	}

	_, e = NewSensorInterface(stubPort1, bells3[:], mockHandler)
	if e == nil {
		t.Errorf("Validity checking of bells is not working for oversized bells array")
	}

	_, e = NewSensorInterface(stubPort1, bells1[:], nil)
	if e == nil {
		t.Errorf("Validity checking of handler is not working")
	}
}

// Testing of Handling functionality using mock Handler and Port

func TestHandler(t *testing.T) {
	mr := mockRinger{}

	var bells1 [2]Bell // Define 2 bells

	sensor1, _ := NewBellSensor('1', 30)
	sensor2, _ := NewBellSensor('2', 30)

	bell1, _ := NewBell("1", sensor1, &mr)
	bell2, _ := NewBell("2", sensor2, &mr)

	bells1[0] = *bell1
	bells1[1] = *bell2

	testChan := make(chan byte)
	mockPort := newMockPort(testChan)

	resultChan := make(chan *Bell)
	mockHandler := newMockHandler(resultChan)

	// Now run tests against mockPort
	go handlerTester("12", testChan, resultChan)

	// Create SensorInterface
	si1, _ := NewSensorInterface(mockPort, bells1[:], mockHandler)

	si1.Start()

}

type mockPort struct {
	testChan chan byte
}

func newMockPort(testChan chan byte) *mockPort {
	return &mockPort{testChan: testChan}
}

func (mp *mockPort) Read(p []byte) (int, error) {

	rxChar, more := <-mp.testChan // Get next available character from channel
	// When the channel is closed, the test ends
	if !more {
		return 0, io.EOF
	}
	buf := make([]byte, 1)
	buf[0] = rxChar
	log.Printf("Received character '%s'\n", string(rxChar))
	copy(p, buf)
	return 1, nil
}

type mockHandler struct {
	resultChan chan *Bell
	BellHandler
}

func newMockHandler(resultChan chan *Bell) *mockHandler {
	return &mockHandler{resultChan: resultChan}
}

func (mh *mockHandler) Handle(bell *Bell) {
	if bell != nil {
		log.Printf("Received Bell: %s with a delay of %d\n", bell.Name, bell.Sensor.Delay)
		mh.resultChan <- bell
	}
}

// TODO: This needs to accept a map or array of tests/results and then perform them
func handlerTester(testString string, testChan chan byte, resultChan chan *Bell) {
	log.Printf("handlerTester running")
	for i := range testString {
		testChar := testString[i]
		log.Printf("Sending character '%s'\n", string(testChar))
		testChan <- testChar
		bell := <-resultChan
		log.Printf("Received bell '%s'\n", bell.Name)
	}
	log.Printf("End of test, close channel\n")
	close(testChan)
}
