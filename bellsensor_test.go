package bellsimlib

import (
	"fmt"
	"testing"
)

func TestNewBellSensor(t *testing.T) {
	var testID byte = '1'
	var testDelay uint = 30

	// Test basic construction
	sensor1, e := NewBellSensor(testID, testDelay) // Correctly defined

	if e != nil {
		t.Error("BellSensor did not get correctly created")
	}

	if sensor1.Identifier != testID {
		t.Errorf("Identifier was incorrect, expected: %d, actual: %d", testID, sensor1.Identifier)
	}
	if sensor1.Delay != testDelay {
		t.Errorf("Delay was incorrect, expected: %d, actual: %d", testDelay, sensor1.Delay)
	}
}

func TestIsValidMbiChar(t *testing.T) {
	testCases := []struct {
		Char    byte
		IsValid bool
	}{
		{'1', true},
		{'2', true},
		{'3', true},
		{'4', true},
		{'5', true},
		{'6', true},
		{'7', true},
		{'8', true},
		{'9', true},
		{'0', true},
		{'E', true},
		{'T', true},
		{'A', true},
		{'B', true},
		{'C', true},
		{'D', true},
		{'F', false},
		{'!', false},
		{'a', false},
	}

	for _, testCase := range testCases {
		testName := fmt.Sprintf("%d", testCase.Char)
		t.Run(testName, func(t *testing.T) {
			result := IsValidMbiChar(testCase.Char)

			if result != testCase.IsValid {
				t.Errorf("expected result to be %t, but got %t", testCase.IsValid, result)
			}
		})
	}
}
