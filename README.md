# Bell Simulator library

Bellsimlib provides a useful library to interface with bell sensors and build simulator apps

For interfacing to the bell sensors via an MBI serial interface you will need the go serial library.
To make audible bell sounds you will need the fluidsynth library which, on Linux based distros, will
require libfluidsynth1 to be installed.

On Ubuntu, Raspbian and other Debian based Linux distros you should be able to install this as follows:

``` bash
sudo apt install libfluidsynth1
sudo apt install libfluidsynth1-dev
```

You can then install the Go libraries:

``` bash
go get github.com/jacobsa/go-serial/serial
go get github.com/sqweek/fluidsynth
```

To use this library in your own applications, and you are using go modules, then
you should just need to import the library:

```go
import (
    "gitlab.com/kamilnerBells/bellsimlib"
)
```

If you aren't using go modules, or wish to manually install the library, use:

``` bash
go get gitlab.com/kamilnerBells/bellsimlib
```

For most applications (and for the example app), you will want the scheduler. To
manually install this, use:

```bash
go get gitlab.com/kamilnerBells/scheduler
```

To run the example:

```bash
cd examples
go run demotouch.go
```
