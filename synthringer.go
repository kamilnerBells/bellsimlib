package bellsimlib

import (
	"time"

	"github.com/sqweek/fluidsynth"
	"gitlab.com/kamilnerBells/scheduler"
)

/*
 * SynthRinger needs to implement a MIDI Synth ringer using Soundfonts
 * This needs to include the use of a scheduler to schedule note on and off
 * events into the future.
 * This needs to have a map of bell names to MIDI note values
 */

// SynthRinger implements a Soundfont Synthesizer based ringer
type SynthRinger struct {
	fluid    fluidsynth.Synth // The Fuidsynth player
	midiChan uint8            // MIDI channel to use
	notes    map[string]uint8 // Bell to MIDI note allocations
	//schedule chan ScheduledSynthEvent // Note events are sent via this channel for scheduling
	latency   uint          // Latency of sound system in ms (informational metadata)
	velocity  uint8         // Velocity of notes
	duration  time.Duration // Duration of notes
	scheduler *scheduler.Scheduler
}

// NoteEvent is a struct for MIDI Note events
type NoteEvent struct {
	synth          SynthRinger
	note, velocity uint8
}

// NewSynthRinger function is a constructor for SynthRinger
func NewSynthRinger(polyphony int, sfont string, midiChan uint8, latency uint) (SynthRinger, error) {

	sched, _ := scheduler.Init(latency)
	// Initialisation of Synth here
	settings := fluidsynth.NewSettings()
	settings.SetString("audio.driver", "alsa")
	settings.SetString("audio.alsa.device", "default")
	settings.SetInt("synth.polyphony", polyphony)
	settings.SetNum("synth.gain", 1)

	synth := SynthRinger{
		fluid:     fluidsynth.NewSynth(settings),
		midiChan:  midiChan,
		notes:     make(map[string]uint8),
		scheduler: sched,
		latency:   latency,
		duration:  200,
		velocity:  64,
	}
	synth.fluid.SFLoad(sfont, true)
	fluidsynth.NewAudioDriver(settings, synth.fluid)

	return synth, nil
}

// SetNote allows the MIDI note to be configured for a bell
func (sr SynthRinger) SetNote(name string, note uint8) {
	sr.notes[name] = note
}

// Ring rings a bell using the synthesizer
func (sr SynthRinger) Ring(rd RingData) {
	//log.Printf("SynthRinger is ringing Bell: %s with a delay of %d\n", rd.Name, rd.Delay)
	midiNote := sr.notes[rd.Name]
	noteOn := scheduler.NewScheduledEvent(NoteEvent{sr, midiNote, sr.velocity}, time.Duration(rd.Delay))
	noteOff := scheduler.NewScheduledEvent(NoteEvent{sr, midiNote, 0}, time.Duration(rd.Delay)+sr.duration*time.Millisecond)
	scheduler.ScheduleEvent(sr.scheduler, noteOn)
	scheduler.ScheduleEvent(sr.scheduler, noteOff)
}

// RenderEvent renders the event
func (ev NoteEvent) RenderEvent() {
	/*
	 * Here we receive either note on or note off plus a (MIDI) channel, plus a note value
	 * and we should call the note on or off function accordingly
	 */
	if ev.velocity > 0 {
		ev.synth.fluid.NoteOn(ev.synth.midiChan, ev.note, ev.velocity)
	} else {
		ev.synth.fluid.NoteOff(ev.synth.midiChan, ev.note)
	}
}
