module gitlab.com/kamilnerBells/bellsimlib

go 1.13

require (
	github.com/sqweek/fluidsynth v0.0.0-20151217121601-462ff346de79
	gitlab.com/kamilnerBells/scheduler v0.0.0-20190917131843-d00fddd77f2d
	gopkg.in/yaml.v2 v2.2.3
)
