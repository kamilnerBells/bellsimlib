package bellsimlib

import (
	"errors"
)

// RingData defines the data required by a ringer in order to perform the ring
type RingData struct {
	Name       string
	Handstroke bool
	Delay      uint
}

// Ringer interface defines the methods for a ringer implementation
type Ringer interface {
	Ring(RingData) // Ring the bell, takes bell name, stroke and delay
}

// Bell defines the characteristics of a bell
type Bell struct {
	Name             string     // A name for the bell, such as "treble" or just "1"
	Sensor           BellSensor // Bell sensor
	Struckness       uint       // How evenly struck the bell is as a percentage (50% is completely balanced)
	isHandstrokeNext bool       // Bell state: true if the next stroke is a hand stroke, false if next stroke is back stroke
	ringer           Ringer     // Bell ringer implementation
}

// NewBell function is a constructor for Bell
func NewBell(name string, sensor *BellSensor, ringer Ringer) (*Bell, error) {
	/*
	 * Checks to perform:
	 *  1. Name needs to be at least 1 character and less than 10
	 *  2. Sensor needs to be not null
	 *  3. Pitch need to be a valid value. These should be in MIDI note format
	 *
	 */
	const defaultStruckness = 50 // Default struckness is 50%
	var bell *Bell

	if len(name) < 1 {
		return bell, errors.New("bellsimlib: Bell name must be at least 1 character")
	}

	if len(name) > 10 {
		return bell, errors.New("bellsimlib: Bell name too long, maximum length is 10 characters")
	}
	if *sensor == (BellSensor{}) {
		return bell, errors.New("bellsimlib: Bell sensor must be defined")
	}
	if ringer == nil {
		return bell, errors.New("bellsimlib: ringer must be defined")
	}

	bell = &Bell{name, *sensor, defaultStruckness, true, ringer} // defaults to first stroke being handstroke
	return bell, nil
}

// SetStruckness sets the "struckness" (how well balanced the hand and back strokes are) of the bell
// Struckness is defined as a percentage balance between hand and back strokes with 50 being perfectly balanced
// Values lower than 50 indicate odd-struckness with a longer back stroke period
// Values higher than 50 indicate odd-struckness with a longer hand stroke period
// The value of struckness must be between 40 and 60 inclusive
func (b *Bell) SetStruckness(struckness uint) error {
	if (struckness < 40) || (struckness > 60) {
		return errors.New("bellsimlib: struckness must be from 40 to 60")
	}
	b.Struckness = struckness
	return nil
}

// Ring performs a stroke of the bell
func (b *Bell) Ring() {

	if b.isHandstrokeNext {
		// Calculate bell delay
		var delay = b.Sensor.Delay - ((50 - b.Struckness) / 100 * b.Sensor.Delay) // Calculate for odd-struckness
		//log.Printf("Ringing hand stroke on bell '%s' with a delay of %d\n", b.Name, delay)
		// Here we need to call ringer
		b.ringer.Ring(RingData{b.Name, b.isHandstrokeNext, delay})
	} else {
		// Calculate bell delay
		var delay = b.Sensor.Delay - ((b.Struckness - 50) / 100 * b.Sensor.Delay) // Calculate for odd-struckness
		//log.Printf("Ringing back stroke on bell '%s' with a delay of %d\n", b.Name, delay)
		// Here we need to call the ringer
		b.ringer.Ring(RingData{b.Name, b.isHandstrokeNext, delay})
	}
	b.isHandstrokeNext = !b.isHandstrokeNext // Set to next stroke
}
