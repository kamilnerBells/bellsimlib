# BellHandler Description and notes

This is a brief description of how the BellHandler works.

BellHandler is an interface with a single method: Handle

The Handle method takes a pointer to a Bell struct which contains all of the information about the bell that is to be rung including (at this point) the MIDI note to be played and the Synthesizer object which should play it.

At present it is expected that the Synthesizer will handle scheduling of the appropriate events (note on, note off).

The Handler is called by the Sensor Interface which receives the sensor (or demo) input anychronously through a channel, then matches the received data up to a bell object and calls the Handler.

## Planned changes and thoughts

The plan is to decouple the synthesizer from the handler and use an intermediate rendering class. This can be instantiated to perform synthesizer playback as before, or to perform some other action. A generic scheduler interface is available to support rendering which requires it (such as for MIDI note on/off).

One challenge is that, currently, the bell object is currently passed from the SensorInterface to the Handler. Currently, this contains generic data and state concerning a specific bell (Name, sensor, struckness, isHandStrokeNext) but also contains renderer-specific data (a pointer to the Synth and the note value to use).

For different renderers, different information will be required. For instance, to push to a mobile device via bluetooth, the renderer may need to know the device ID.

Ideally we need to abstract this so that we can pass in either a vanilla object with no rendering information, which requires the rendering-specific information to be separately stored, or we need to be able to extend the bell object with new fields. We cannot extend the struct itself, but we can create a new struct and embed the Bell struct into it and this can still conform to the interface.

(See <https://www.ardanlabs.com/blog/2014/05/methods-interfaces-and-embedded-types.html>)

This probably makes most sense for bell-specific data such as MIDI note or mapped BT device data.

The renderer itself should be common to all bells and should be instantiated and passed in some other way, perhaps to the handler as part of the Handler initialisation.

For instance, instead of: `handler := newSimpleHandler()`
We have: `handler := newSimpleHandler(renderer)`

Where renderer has previously been instantiated. In this case, when the Handle method is called, the BellHandler already knows the renderer.

We should have a renderer interface which can be used for this.
