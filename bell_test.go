package bellsimlib

import (
	"log"
	"testing"
)

func TestNewBell(t *testing.T) {
	mr := mockRinger{}

	var testID byte = '1'
	var testName = "1"

	// Test basic construction
	sensor1, e := NewBellSensor(testID, 30)
	bell1, e := NewBell(testName, sensor1, mr) // Correctly defined
	if e != nil {
		t.Error("Bell did not get correctly created")
	}

	if bell1.Name != testName {
		t.Errorf("Name was incorrect, expected: %s, actual: %s", testName, bell1.Name)
	}

	if bell1.Sensor != *sensor1 {
		t.Errorf("Sensor was incorrect, expected: %v, actual: %v", sensor1, bell1.Sensor)
	}

	// Test invalid name
	_, e1 := NewBell("", sensor1, &mr)            // Test empty name
	_, e2 := NewBell("12345678901", sensor1, &mr) // Test too long a name
	if (e1 == nil) || (e2 == nil) {
		t.Error("Bell name length check is not working")
	}

	// Test invalid sensor
	_, e1 = NewBell("1", (&BellSensor{}), &mr) // Undefined sensor
	if e1 == nil {
		t.Error("Bell sensor validity check is not working")
	}
}

func TestSetStruckness(t *testing.T) {

	mr := mockRinger{}

	var testID byte = '1'
	var testName = "1"

	// Construct a test Bell
	sensor1, _ := NewBellSensor(testID, 30)
	bell1, _ := NewBell(testName, sensor1, &mr) // Correctly defined
	if bell1.Struckness != 50 {
		t.Errorf("Default Struckness was incorrect, expected: %d, actual: %d", 50, bell1.Struckness)
	}

	_ = bell1.SetStruckness(40) // Valid value
	if bell1.Struckness != 40 {
		t.Errorf("Struckness was incorrect, expected: %d, actual: %d", 40, bell1.Struckness)
	}
	_ = bell1.SetStruckness(60) // Valid value
	if bell1.Struckness != 60 {
		t.Errorf("Struckness was incorrect, expected: %d, actual: %d", 60, bell1.Struckness)
	}

	// Test for validity checking
	e1 := bell1.SetStruckness(39) // invalid value, too low
	if (e1 == nil) || (bell1.Struckness == 39) {
		t.Error("Bell struckness validity check lower limit is not working")
		t.Error(e1)
	}

	e2 := bell1.SetStruckness(61) // invalid value, too high
	if (e2 == nil) || (bell1.Struckness == 61) {
		t.Error("Bell struckness validity check upper limit is not working")
	}

}

type mockRinger struct {
}

func (mr mockRinger) Ring(rd RingData) {
	log.Printf("Ringer is ringing Bell: %s with a delay of %d\n", rd.Name, rd.Delay)
}
