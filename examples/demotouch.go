package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/kamilnerBells/bellsimlib"
	"gopkg.in/yaml.v2"
)

type demoTouch struct {
	Touch struct {
		Description   string   `yaml:"description"`
		Sequence      []string `yaml:"sequence"`
		InterBellGap  uint     `yaml:"interBellGap"`
		HandstrokeGap uint     `yaml:"handstrokeGap"`
	} `yaml:"touch"`
}

func main() {

	// Get configuration
	config, err := bellsimlib.LoadConfig("config.yaml")

	if err != nil {
		fmt.Print(err)
		os.Exit(3)
	}

	// Construct bells from configuration

	var sensors []*bellsimlib.BellSensor

	var bells []bellsimlib.Bell

	// Instantiate synth ringer
	sr, _ := bellsimlib.NewSynthRinger(len(config.Bells), config.SynthSoundfont, 1, 12)

	// Instantiate bells based on configuration
	for i, bell := range config.Bells {
		sensor, _ := bellsimlib.NewBellSensor([]byte(bell.SensorChar)[0], bell.SensorDelay)
		sensors = append(sensors, sensor)
		newBell, _ := bellsimlib.NewBell(bell.Name, sensors[i], &sr)
		sr.SetNote(bell.Name, bell.Note) // Set SynthRinger note map based on configuration
		bells = append(bells, *newBell)
	}

	// DEMO STARTS HERE

	demoChan := make(chan byte)
	demoPort := newDemoPort(demoChan)

	handler := newSimpleHandler()

	// Reading in demo touch data
	demoData, err := loadDemo("demoConfig.yaml")

	// Build demo from data
	fmt.Printf("Building demo data for %s", demoData.Touch.Description)
	var demoSeq strings.Builder
	for _, seqline := range demoData.Touch.Sequence {
		demoSeq.WriteString(seqline)
	}

	log.Printf("Handstroke Gap: %d\n", demoData.Touch.HandstrokeGap)

	// Now run data through demoPort
	go demo(demoSeq.String(), demoChan, demoData.Touch.InterBellGap, demoData.Touch.HandstrokeGap)

	// Create a new SensorInterface listening to the output of the demo
	// and concurently run it
	//
	// Currently demoPort is a handler that is passed to the SensorInterface
	// For production we really don't want that. Ideally we need to separate
	// the demo playback from the SensorInterface so that it plays the bells
	// Independently.
	// We can then start the SensorInterface and let it do its thing whilst
	// Concurrently playing any demo data.
	// We also need to consider this in the context of real-world applications
	// At the start it would be good to have a command channel running which can
	// be used to configure things like bell timings concurrently with sensor handling
	// as well as having a command channel to the application to allow it to
	// shut down gracefully
	sh1, _ := bellsimlib.NewSensorInterface(demoPort, bells[:], handler)
	sh1.Start()
	time.Sleep(1000 * time.Millisecond)
}

// simpleHandler is an implementation of a handler
//
// It will receive bell events and output them to the synth
type simpleHandler struct {
	bellsimlib.BellHandler
}

func newSimpleHandler() *simpleHandler {
	return &simpleHandler{}
}

func (mh *simpleHandler) Handle(bell *bellsimlib.Bell) {
	if bell != nil {
		log.Printf("Received Bell: %s\n", bell.Name)
		// Tell bell to ring
		bell.Ring()
	}
}

// demoPort is a mock port for demonstration
//
// It will loop through a string and allow the characters in it to be read one
// at a time whilst adding a configurable delay between characters
type demoPort struct {
	ch chan byte
}

func newDemoPort(ch chan byte) *demoPort {
	return &demoPort{ch: ch}
}

func (dp *demoPort) Read(p []byte) (int, error) {

	rxChar, more := <-dp.ch // Get next available character from channel
	// When the channel is closed, the demo is finished
	if !more {
		return 0, io.EOF
	}
	buf := make([]byte, 1)
	buf[0] = rxChar
	log.Printf("Received character '%s'\n", string(rxChar))
	copy(p, buf)
	return 1, nil
}

func demo(demoString string, demoChan chan byte, delay uint, handstrokeGap uint) {
	for i := range demoString {
		bellChar := demoString[i]
		log.Printf("Sending character '%s'\n", string(bellChar))
		demoChan <- bellChar // Send bell character to demo port to simulate sensor

		delayWait := time.Duration(delay) * time.Millisecond
		// Space waits half the normal duration
		if bellChar == ' ' {
			delayWait = time.Duration(handstrokeGap) * time.Millisecond
		}
		time.Sleep(delayWait)
	}
	log.Printf("End of demo, close the channel\n")
	close(demoChan)
}

// loadDemo - Loads Demo configuration
func loadDemo(filename string) (demoTouch, error) {
	fmt.Println("Loading demo file " + filename)
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return demoTouch{}, err
	}

	var dt demoTouch
	err = yaml.Unmarshal(bytes, &dt)
	if err != nil {
		return demoTouch{}, err
	}

	return dt, nil
}
