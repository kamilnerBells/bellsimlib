# Bellsimlib Examples

This folder is for example applications that use bellsimlib.

## demoTouch

This example application simulates bell sensors on 8 bells ringing a "touch" of Plain Bob Major using a mock to simulate a bell sensor interface.

This example also includes church bell soundfonts and demonstrates how soundfonts are loaded and used.

### Prerequisites

This demo application requires fluidsynth, serial, bellsimlib, and scheduler. The following describes how to install on a Debian distro.

First the Linux libraries required by Fluidsynth should be installed.

This installs Jackd2 libraries which are required for libfluidsynth-dev on recent (22.04) Ubuntu versions. Tying to install fluidsynth-dev without this will try to replace Jack v2 with Jack v1. This can be done as follows:

``` bash
sudo apt install libfluidsynth1
sudo apt install libjack-jackd2-dev
sudo apt install libfluidsynth-dev
```

Then the Go modules should be set up as follows:

``` bash
go get github.com/jacobsa/go-serial/serial
go get github.com/sqweek/fluidsynth
go get gitlab.com/kamilnerBells/bellsimlib
go get gitlab.com/kamilnerBells/scheduler
```

### Running

The demo appllication can be run by executing the following from within the examples folder:
`go run demotouch.go`
